import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'; //plugin cordova
import { ToastController, Platform } from 'ionic-angular'; //Componente
import { HistorialServices } from "../../providers/historial/historial";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    private barcodeScanner: BarcodeScanner,
    public toastCtrl: ToastController,
    private platform: Platform,
    private _historialService:HistorialServices
    ) {}

  scan(){
    if(!this.platform.is('cordova')){
      this._historialService.agregar_historial("http://www.google.cl");
      this.mostrarMensaje("Esta solo en plataforma de pruebas sin acceso a la Camara");
      return;
    }else{


      this.barcodeScanner.scan().then((barcodeData) => {
        console.log("Data del Scan: ", barcodeData.text);
        this.mostrarMensaje(barcodeData.text);
        console.log("result: ",barcodeData.text);
        console.log("format: ",barcodeData.format);
        console.log("cancelled: ",barcodeData.cancelled);

        if( barcodeData.cancelled == false && barcodeData.text != null ){
          this._historialService.agregar_historial(barcodeData.text);
        }


      }, (err) => {
        console.log("Error del Scanner: ",err);
        this.mostrarMensaje(err);
      });

    }
  }

  mostrarMensaje(mensaje:string) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000
    });
    toast.present();
  }

}
